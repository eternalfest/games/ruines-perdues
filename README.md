# Ruines Perdues

Clone with HTTPS:
```shell
git clone --recurse-submodules https://gitlab.com/eternalfest/games/ruines-perdues.git
```

Clone with SSH:
```shell
git clone --recurse-submodules git@gitlab.com:eternalfest/games/ruines-perdues.git
```
