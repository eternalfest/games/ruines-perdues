import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import vault.Vault;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import ruines.Items;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    items: Items,
    game_params: GameParams,
    noNextLevel: NoNextLevel,
    vault: Vault,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
