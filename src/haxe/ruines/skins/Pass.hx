package ruines.skins;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
import vault.ISkin;

class Pass implements ISkin {
  public static var ID_PASS: Int = 238;

  public var id(default, null): Int = 1239;
  public var sprite(default, null): Null<String> = null;

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_PASS + 1));
    var filter: ColorMatrixFilter = ColorMatrix.fromHsv(100, 1, 1).toFilter();
    mc.filters = [filter];
  }
}