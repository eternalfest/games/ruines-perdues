package ruines;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.ISkin;
import ruines.skins.Pass;
import ruines.skins.Truc;

@:build(patchman.Build.di())
class Items {
  @:diExport
  public var skins(default, null): FrozenArray<ISkin>;

  public function new() {
    this.skins = FrozenArray.of(
      new Pass(),
      new Truc()
    );
  }
}